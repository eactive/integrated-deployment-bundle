<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\DeployTools;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;

use Integrated\Bundle\UserBundle\Model\Group;
use Integrated\Bundle\WorkflowBundle\Entity\Definition\Permission;
use Integrated\Bundle\WorkflowBundle\Entity\Definition\State;

/**
 * @author Johan Liefers <johan@e-active.nl>
 */
class WorkflowTool
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        if (!class_exists('Integrated\Bundle\WorkflowBundle\IntegratedWorkflowBundle')) {
            throw new \RuntimeException('IntegratedWorkflowBundle must be loaded to use this tool');
        }
        
        if (!class_exists('Integrated\Bundle\UserBundle\IntegratedUserBundle')) {
            throw new \RuntimeException('IntegratedUserBundle must be loaded to use this tool');
        }

        $this->em = $em;
    }

    /**
     * @param Group $group
     * @param $workflowName
     * @param int $mask (3 = Permission::READ + Permission::WRITE)
     * @throws EntityNotFoundException
     */
    public function addPermissionForGroup(Group $group, $workflowName, $mask = 3)
    {
        $workflow = $this->em->getRepository('IntegratedWorkflowBundle:Definition')
            ->findOneBy(['name' => $workflowName]);

        if (!$workflow) {
            throw new EntityNotFoundException('Workflow not found');
        }

        foreach ($workflow->getStates() as $state) {
            $this->addPermission($group, $state, $mask);
        }
    }

    /**
     * @param Group $group
     * @param State $state
     * @param int $mask (3 = Permission::READ + Permission::WRITE)
     * @return Permission
     */
    public function addPermission(Group $group, State $state, $mask = 3)
    {
        $permission = $this->em->getRepository(Permission::class)->findOneBy(['group' => $group, 'state' => $state]);
        if ($permission instanceof Permission) {
            return $permission;
        }

        $permission = new Permission();
        $permission->setGroup($group)
            ->setMask($mask)
            ->setState($state);

        $this->em->persist($permission);
        $this->em->flush($permission);

        return $permission;
    }
}
