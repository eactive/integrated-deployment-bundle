<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\DeployTools;

use Doctrine\ORM\EntityManager;

use Integrated\Bundle\UserBundle\Model\Group;
use Integrated\Bundle\UserBundle\Model\Role;

/**
 * @author Johan Liefers <johan@e-active.nl>
 */
class SecurityUserTool
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        if (!class_exists('Integrated\Bundle\UserBundle\IntegratedUserBundle')) {
            throw new \RuntimeException('IntegratedUserBundle must be loaded to use this tool');
        }

        $this->em = $em;
    }

    /**
     * @param string $groupName
     * @param array $roles
     * @param string|null $label
     * @param string|null $description
     * @return Group
     */
    public function createGroupAndRoles($groupName, array $roles, $label = null, $description = null)
    {
        $group = $this->createGroup($groupName);
        
        foreach ($roles as $role) {
            $role = $this->createRole($role, $label, $description);
            if (!$group->hasRole($role)) {
                $group->addRole($role);
            }
        }

        $this->em->flush($group);

        return $group;
    }

    /**
     * @param string $role
     * @param string|null $label
     * @param string|null $description
     * @return Role
     */
    public function createRole($role, $label = null, $description = null)
    {
        //check if role doesn't already exist
        $userRole = $this->em->getRepository(Role::class)->findOneBy(['role' => strtoupper($role)]);
        if ($userRole instanceof Role) {
            return $userRole;
        }
        
        $userRole = new Role($role, $label, $description);

        $this->em->persist($userRole);
        $this->em->flush($userRole);

        return $userRole;
    }

    /**
     * @param $groupName
     * @return Group
     */
    public function createGroup($groupName)
    {
        //check if $group doesn't already exist
        $group = $this->em->getRepository(Group::class)->findOneBy(['name' => strtoupper($groupName)]);
        if ($group instanceof Group) {
            return $group;
        }
        
        $group = new Group();
        $group->setName($groupName);

        $this->em->persist($group);
        $this->em->flush($group);

        return $group;
    }
}
