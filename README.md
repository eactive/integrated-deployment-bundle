# IntegratedDeploymentBundle #
This bundle provides deploy tools

## Requirements ##
* See the require section in the composer.json

## Features ##
* Copy mongo and mysql databases
* Deploy tools for updating Integrated content
* Abstract migrations command with handy functions for updating Integrated content using Doctrine migrations

## Documentation ##
* [Integrated for Developers](http://integratedfordevelopers.com/ "Integrated for Developers")

## Installation ##
This bundle can be installed following these steps:

### Install using composer ###

    $ php composer.phar require integrated/deployment-bundle:~0.5

### Enable the bundle ###

    // app/AppKernel.php
    public function registerBundles()
    {
        return array(
            // ...
            new Integrated\Bundle\DeploymentBundle\IntegratedDeploymentBundle()
            // ...
        );
    }

## License ##
This bundle is under the MIT license. See the complete license in the bundle:

    LICENSE

## Contributing ##
Pull requests are welcome. Please see our [CONTRIBUTING guide](http://integratedfordevelopers.com/contributing "CONTRIBUTING guide").

## About ##
This bundle is part of the Integrated project. You can read more about this project on the
[Integrated for Developers](http://integratedfordevelopers.com/ "Integrated for Developers") website.