<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\Util;

use Doctrine\ODM\MongoDB\DocumentManager;

use Integrated\Bundle\ContentBundle\Document\Content\Content;
use Integrated\Bundle\ContentBundle\Document\Relation\Relation;

/**
 * Converts mongo references to integrated relations without hydration, so you can already remove the odm mapping
 * @author Johan Liefers <johan@e-active.nl>
 */
class MongoRefConverter
{
    /**
     * @var string|null
     */
    protected $dbName = null;

    /**
     * @var \Doctrine\MongoDB\Collection|null
     */
    protected $collection = null;

    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * MongoRefConverter constructor.
     * @param DocumentManager $dm
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @internal Only works for referenceOne references
     * @param string $class
     * @param array $conversions key is old mongo relation, value is integrated relation
     */
    public function convertDocuments($class, array $conversions)
    {
        foreach ($this->getDocuments($class) as $document) {
            $this->convertDocument($document, $conversions);
        }
    }

    /**
     * @internal Only works for referenceOne references
     * @param array $document
     * @param array $conversions key is old mongo relation, value is integrated relation
     * @throws \Exception
     */
    public function convertDocument(array $document, array $conversions)
    {
        /**
         * @var string $oldRelation
         * @var Relation $integratedRelation
         */
        foreach ($conversions as $oldRelation => $integratedRelation) {
            if (!$integratedRelation instanceof Relation) {
                throw new \Exception('Invalid conversion argument, should be key value, with value being integrated relation');
            }

            if (isset($document[$oldRelation]['$id']) && isset($document[$oldRelation]['class'])) {
                //remove existing relation, probably not necessary, but just to be sure
                if (isset($document['relations'])) {
                    foreach ($document['relations'] as $key => $relation) {
                        if ($integratedRelation->getType() === $relation['relationType']) {
                            unset($document['relations'][$key]);

                            $document['relations'] = array_values($document['relations']);
                        }
                    }
                }

                //set integrated relation
                $document['relations'][] = [
                    'relationId' => $integratedRelation->getId(),
                    'relationType' => $integratedRelation->getType(),
                    'references' => [[
                        '$ref' => 'content',
                        '$id' => $document[$oldRelation]['$id'],
                        '$db' => $this->getDatabaseName(),
                        'class' => $document[$oldRelation]['class'],
                    ]]
                ];

                unset($document[$oldRelation]);
            }
        }

        $this->getCollection()->update(['_id' => $document['_id']], $document);
    }


    /**
     * @param string $class
     * @return \Doctrine\MongoDB\Iterator
     */
    protected function getDocuments($class)
    {
        return $this->dm->getRepository($class)->createQueryBuilder()
            ->hydrate(false)
            ->getQuery()->getIterator();
    }

    /**
     * @return string
     */
    protected function getDatabaseName()
    {
        if (null === $this->dbName) {
            $this->dbName = $this->dm->getDocumentDatabase(Content::class)->getName();
        }

        return $this->dbName;
    }

    /**
     * @return \Doctrine\MongoDB\Collection
     */
    protected function getCollection()
    {
        if (null === $this->collection) {
            $this->collection = $this->dm->getConnection()->selectCollection($this->getDatabaseName(), 'content');
        }

        return $this->collection;
    }
}
