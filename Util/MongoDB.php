<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\Util;

use Doctrine\MongoDB\Connection;
use Doctrine\MongoDB\Collection;

use RuntimeException;

/**
 * @author Jan Sanne Mulder <jansanne@e-active.nl>
 */
class MongoDB
{
    /**
     * Copy the collections from the source to the target database.
     *
     * @param Connection $connection
     * @param string     $target
     * @param string     $source
     * @param string     $host
     * @param string     $username
     * @param string     $password
     *
     * @trows RuntimeException
     */
    public static function copy(Connection $connection, $target, $source, $host = null, $username = null, $password = null)
    {
        self::clear($connection, $target);

        $command = [
            'copydb' => 1,
            'todb'   => $target,
            'fromdb' => $source,
        ];

        $admin = $connection->selectDatabase('admin');

        if ($host) {
            $command['fromhost'] = $host;

            if ($username || $password) {
                $nonce = self::processCommandResult($admin->command(['copydbgetnonce' => 1, 'fromhost' => $host]));

                $command['username'] = $username;
                $command['nonce'] = $nonce;
                $command['key'] = md5($nonce . $username . md5($username . ':mongo:' . $password));

                unset($nonce);
            }
        }

        self::processCommandResult($admin->command($command, ['timeout' => -1]));
    }

    /**
     * Remove all the collections from the database.
     *
     * @param Connection $connection
     * @param string     $target
     *
     * @trows RuntimeException
     */
    public static function clear(Connection $connection, $target)
    {
        $database = $connection->selectDatabase($target);

        /** @var Collection $collection */

        foreach ($database->listCollections() as $collection) {
            self::processCommandResult($collection->drop());
        }
    }

    /**
     * Process the mongo command result.
     *
     * Will return a exception when the result got a error message
     * Will return a scalar if the result only contains one variable
     * Will return true if the result contains no variable
     *
     * Else it will return a array with all the result content
     *
     * @param array $result
     *
     * @return mixed
     *
     * @trows RuntimeException
     */
    private static function processCommandResult(array $result)
    {
        if ($result['ok'] == 1) {
            unset($result['ok']);

            if (count($result) == 0) {
                return true;
            }

            if (count($result) == 1) {
                $result = array_shift($result);
            }

            return $result;
        }

        throw new RuntimeException(sprintf('MongoDB returned with a error "%s"', $result['errmsg']));
    }
}