<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\Util;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

/**
 * @author Jan Sanne Mulder <jansanne@e-active.nl>
 */
class MySQL
{
    /**
     * Copy the tables and content from the source to the target database.
     *
     * @param Connection $target
     * @param Connection $source
     * @param array $excludeTables
     *
     * @throws DBALException
     */
    public static function copy(Connection $target, Connection $source, array $excludeTables = [], OutputInterface $output = null)
    {
        self::clear($target);

        // mysqldump the source database and pipe that to the target database

        $source = $source->getParams();

        if (isset($source['master'])) {
            $source = $source['master'];
        }

        $command[] = 'mysqldump';
        $command[] = '--single-transaction';
        $command[] = '--quick';

        if (!empty($excludeTables)) {
            foreach ($excludeTables as $excludeTable) {
                $command[] = sprintf('--ignore-table="%s"', $excludeTable);
            }
        }

        if (!empty($source['user'])) {
            $command[] = sprintf('--user="%s"', $source['user']);
        }

        if (!empty($source['user'])) {
            $command[] = sprintf('--password="%s"', $source['password']);
        }

        if (!empty($source['host'])) {
            $command[] = sprintf('--host="%s"', $source['host']);
        }

        if (!empty($source['port'])) {
            $command[] = sprintf('--port="%s"', $source['port']);
        }

        $command[] = $source['dbname'];
        $command[] = '|';

        $target = $target->getParams();

        if (isset($target['master'])) {
            $target = $target['master'];
        }

        $command[] = 'mysql';

        if (!empty($target['user'])) {
            $command[] = sprintf('--user="%s"', $target['user']);
        }

        if (!empty($target['user'])) {
            $command[] = sprintf('--password="%s"', $target['password']);
        }

        if (!empty($target['host'])) {
            $command[] = sprintf('--host="%s"', $target['host']);
        }

        if (!empty($target['port'])) {
            $command[] = sprintf('--port="%s"', $target['port']);
        }

        $command[] = $target['dbname'];

        $process = new Process(implode(' ', $command));
        $process->setTimeout(null);

        $process->run(function ($type, $buffer) use ($output) {
            if ($output) {
                $output->write($buffer);
            }
        });

        if (!$process->isSuccessful()) {
            throw new \RuntimeException($process->getErrorOutput());
        }
    }

    /**
     * Remove all the table from the database.
     *
     * @param Connection $target
     *
     * @throws DBALException
     */
    public static function clear(Connection $target)
    {
        $foreign_key_checks = $target->executeQuery('SELECT @@foreign_key_checks')->fetchColumn(0);

        if ($foreign_key_checks) {
            $target->executeUpdate('SET foreign_key_checks = 0');
        }

        $database = $target->executeQuery('SELECT DATABASE()')->fetchColumn(0);
        $platform = $target->getDatabasePlatform();

        foreach ($target->executeQuery($platform->getListViewsSQL($database)) as $row) {
            $target->executeUpdate($platform->getDropViewSQL($platform->quoteIdentifier($row['TABLE_NAME'])));
        }

        foreach ($target->executeQuery($platform->getListTablesSQL($database)) as $row) {
            $target->executeUpdate($platform->getDropTableSQL($platform->quoteIdentifier(array_shift($row))));
        }

        if ($foreign_key_checks) {
            $target->executeUpdate('SET foreign_key_checks = 1');
        }
    }
}
