<?php

/*
* This file is part of the Integrated package.
*
* (c) e-Active B.V. <integrated@e-active.nl>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Integrated\Bundle\DeploymentBundle\Command;

use Symfony\Component\Process\Process;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Migrations\AbstractMigration as BaseAbstractMigration;

/**
 * @author Johan Liefers <johan@e-active.nl>
 */
abstract class AbstractMigration extends BaseAbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return \Integrated\Bundle\DeploymentBundle\DeployTools\SecurityUserTool
     */
    protected function getSecurityUserTool()
    {
        return $this->container->get('integrated_deployment.deploy_tools.security_user');
    }

    /**
     * @return \Integrated\Bundle\DeploymentBundle\DeployTools\WorkflowTool
     */
    protected function getWorkflowTool()
    {
        return $this->container->get('integrated_deployment.deploy_tools.workflow');
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->container->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * @param string $command
     * @param null|string $env default is current environment, unless set in this variable
     * @param bool $showOutput
     * @param bool $throwExpeption
     */
    protected function exec($command, $env = null, $showOutput = true, $throwExpeption = true)
    {
        $process = new Process(
            'php ' . $this->getRootDir() . '/app/console ' . $command .
            ' --env=' . (null === $env ? $this->getEnvironment() : $env)
        );
        $process->setTimeout(0);

        $process->run(function ($type, $buffer) use ($showOutput, $throwExpeption) {
            if (Process::ERR === $type) {
                if ($throwExpeption) {
                    throw new \RuntimeException('Error executing command: ' . $buffer);
                }
            }
            if ($showOutput) {
                echo $buffer;
            }
        });
    }

    /**
     * @return string
     */
    protected function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }

    /**
     * @return string
     */
    protected function getRootDir()
    {
        return realpath($this->container->get('kernel')->getRootDir() . '/..');
    }
}