<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\Command;

use Doctrine\MongoDB\Connection;

use Integrated\Bundle\DeploymentBundle\Util\MongoDB;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\Process\Process;

/**
 * @author Jan Sanne Mulder <jansanne@e-active.nl>
 */
class CopyMongoDbCommand extends ContainerAwareCommand
{
    /**
     * @var array
     */
    protected $excludedCollections = [
        'content_history',
    ];

    protected function configure()
    {
        $this
            ->setName('deploy:copy:mongodb')

            ->addArgument('source', InputArgument::REQUIRED, 'The source Mongodb database to extract the data from')

            ->addOption('connection', null, InputOption::VALUE_REQUIRED, 'The MongoDB connection to use as the target database')
            ->addOption('host', null, InputOption::VALUE_REQUIRED, 'The MongoDB host of the source database')
            ->addOption('port', null, InputOption::VALUE_REQUIRED, 'The MongoDB port of the source database')
            ->addOption('username', 'u', InputOption::VALUE_REQUIRED, 'The username for connecting with the source MongoDB database')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'The password for connecting with the source MongoDB database')

            ->addOption('force', null, InputOption::VALUE_NONE, 'Set this parameter to execute this action')

            ->setDescription('The command will replace the content of the target MongoDB database with a copy of the source MongoDB database');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $this->getConnection($input);
        $connection->connect();

        $database = null;

        if (method_exists($connection->getConfiguration(), 'getDefaultDB')) {
            $database = $connection->getConfiguration()->getDefaultDB();
        }

        if (!$database) {
            throw new InvalidArgumentException("Connection does not contain a default database.");
        }

        if ($input->getOption('force')) {
            $this->executeWarning($output, 'This command will completely destroy the content of the target MongoDB database', 3);
            $this->executeCopy($output, $connection, $database, $input->getArgument('source'), $input);
        } else {
            $output->writeln('<error>ATTENTION:</error> This operation should not be executed in a production environment.');
            $output->writeln('');
            $output->writeln(sprintf('<info>Would destroy all the data in the database named <comment>%s</comment>.</info>', $database));
            $output->writeln('Please run the operation with --force to execute');
            $output->writeln('<error>All data will be lost!</error>');

            return 1;
        }

        return 0;
    }

    /**
     * @param OutputInterface $output
     * @param Connection      $connection
     * @param string          $target
     * @param string          $source
     * @param InputInterface  $input
     */
    protected function executeCopy(OutputInterface $output, Connection $connection, $target, $source, InputInterface $input)
    {
        // We start out with an array of commands which contains nothing
        $arrCommand = [];

        // We want to start a mongodump command, which is a utility for creating a binary export of the contents of a database (https://docs.mongodb.com/manual/reference/program/mongodump/)
        $arrCommand[] = 'mongodump --out dump';

        // The host contains the resolvable hostname for the mongod to which to connect
        if ($host = $input->getOption('host')) {
            $arrCommand[] = sprintf('--host="%s"', $host);
        }

        // The port specifies the TCP port on which the MongoDB instance listens for client connections
        if ($port = $input->getOption('port')) {
            $arrCommand[] = sprintf('--port="%s"', $port);
        }

        $arrCommand[] = sprintf('--db="%s"', $source);

        if ($username = $input->getOption('username')) {
            $arrCommand[] = sprintf('--username="%s"', $username);
        }

        if ($password = $input->getOption('password')) {
            $arrCommand[] = sprintf('--password="%s"', $password);
        }

        if (!empty($this->excludedCollections)) {
            foreach ($this->excludedCollections as $excludedCollection) {
                $arrCommand[] = sprintf('--excludeCollection="%s"', $excludedCollection);
            }
        }

        $arrCommand[] = '&&';

        $arrCommand[] = 'mongorestore --drop';
        $arrCommand[] = sprintf('dump/%s', $source);

        $arrCommand[] = sprintf('--db="%s"', 'integrated');

        $arrCommand[] = sprintf('--host="%s"', 'mongo');

        // Set the password for the local mongoDB
        $arrCommand[] = sprintf('--password="%s"', 'integrated');

        // Set the username for the local mongoDB
        $arrCommand[] = sprintf('--username="%s"', 'integrated');

        // Set the authenticationDatabase for the local mongoDB
        $arrCommand[] = sprintf('--authenticationDatabase="%s"', 'integrated');

        $arrCommand[] = '&&';

        // Remove dump files
        $arrCommand[] = sprintf('rm -rf dump/%s', $source);
        $strCommand = implode(' ', $arrCommand);

        $processObject = new Process($strCommand);
        $processObject->setTimeout(null)
            ->run(function ($typeObject, $bufferObject) {
                echo $bufferObject;
            });
    }

    /**
     * @param OutputInterface $output
     * @param string          $message
     * @param int             $seconds
     */
    protected function executeWarning(OutputInterface $output, $message, $seconds)
    {
        $padding = 0;

        for ($remaining = $seconds; $remaining > 0; $remaining--) {
            $line = sprintf($message . ', <error>%d</error> seconds remaining to abort', $remaining);

            $output->write("\r");
            $output->write(str_pad($line, $padding = max($padding, strlen($line))));

            sleep(1);
        }

        $output->write("\r");
        $output->writeln(str_pad($message . '.', $padding));
    }

    /**
     * @param InputInterface $input
     *
     * @return Connection
     */
    private function getConnection(InputInterface $input)
    {
        $name = $input->getOption('connection') ?: 'default';
        $service = 'doctrine_mongodb.odm.' . $name . '_connection';

        $container = $this->getContainer();

        if ($container->has($service)) {
            if (($instance = $container->get($service)) instanceof Connection) {
                return $instance;
            }

            throw new InvalidArgumentException(sprintf('The service %s is not an instance of "Doctrine\\MongoDB\\Connection"', $service));
        }

        throw new InvalidArgumentException(sprintf('Could not find a MongoDB connection with the name "%s"', $name));
    }

    /**
     * @param array $result
     *
     * @return mixed
     */
    private function getResult(array $result)
    {
        if ($result['ok'] == 1) {
            unset($result['ok']);

            if (count($result) == 0) {
                return true;
            }

            if (count($result) == 1) {
                $result = array_shift($result);
            }

            return $result;
        }

        throw new RuntimeException(sprintf('MongoDB returned with a error "%s"', $result['errmsg']));
    }
}
