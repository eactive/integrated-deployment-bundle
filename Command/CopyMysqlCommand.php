<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\DeploymentBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;

use Integrated\Bundle\DeploymentBundle\Util\MySQL;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use InvalidArgumentException;

/**
 * @author Jan Sanne Mulder <jansanne@e-active.nl>
 */
class CopyMysqlCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('deploy:copy:mysql')

            ->addArgument('source', InputArgument::REQUIRED, 'The source database to extract the data from')

            ->addOption('connection', null, InputOption::VALUE_REQUIRED, 'The DBAL connection to use as the target database')
            ->addOption('host', null, InputOption::VALUE_REQUIRED, 'The database host of the source database', 'localhost')
            ->addOption('port', null, InputOption::VALUE_REQUIRED, 'The database port of the source database')
            ->addOption('username', 'u', InputOption::VALUE_REQUIRED, 'The username for connecting with the source database')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'The password for connecting with the source database')

            ->addOption('exclude-tables', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Tables to be excluded, needs to be prepended with the databasename')

            ->addOption('force', null, InputOption::VALUE_NONE, 'Set this parameter to execute this action')

            ->setDescription('The command will replace the content of the target database with a copy of the source database');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $target = $this->getTargetConnection($input);
        $target->connect();

        $params = $target->getParams();

        if (isset($params['master'])) {
            $params = $params['master'];
        }

        $database = isset($params['dbname']) ? $params['dbname'] : null;

        if (!$database) {
            throw new InvalidArgumentException("Connection does not contain a 'dbname' parameter.");
        }

        $source = $this->getSourceConnection($input);
        $source->connect();

        $this->executeCompare($output, $source, $target);

        if ($input->getOption('force')) {
            $this->executeWarning($output, 'This command will completely destroy the content of the target database', 3);
            $this->executeCopy($output, $input, $target, $source);
        } else {
            $output->writeln('<error>ATTENTION:</error> This operation should not be executed in a production environment.');
            $output->writeln('');
            $output->writeln(sprintf('<info>Would destroy all the data in the database named <comment>%s</comment>.</info>', $database));
            $output->writeln('Please run the operation with --force to execute');
            $output->writeln('<error>All data will be lost!</error>');

            return 1;
        }

        return 0;
    }

    /**
     * @param OutputInterface $output
     * @param Connection      $target
     * @param Connection      $source
     */
    protected function executeCompare(OutputInterface $output, Connection $target, Connection $source)
    {
        $target = $target->getParams();

        if (isset($target['master'])) {
            $target = $target['master'];
        }

        $target['host'] = gethostbyname($target['host']);

        $source = $source->getParams();

        if (isset($source['master'])) {
            $source = $source['master'];
        }

        $source['host'] = gethostbyname($source['host']);

        foreach (['driver', 'host', 'port', 'dbname'] as $key) {
            if ($target[$key] != $source[$key]) {
                return;
            }
        }

        throw new InvalidArgumentException("The target and source database are the same.");
    }

    /**
     * @param OutputInterface $output
     * @param InputInterface  $input
     * @param Connection      $target
     * @param Connection      $source
     */
    protected function executeCopy(OutputInterface $output, InputInterface $input, Connection $target, Connection $source)
    {
        MySQL::copy($target, $source, $input->getOption('exclude-tables'), $output);
    }

    /**
     * @param OutputInterface $output
     * @param string          $message
     * @param int             $seconds
     */
    protected function executeWarning(OutputInterface $output, $message, $seconds)
    {
        $padding = 0;

        for ($remaining = $seconds; $remaining > 0; $remaining--) {
            $line = sprintf($message . ', <error>%d</error> seconds remaining to abort', $remaining);

            $output->write("\r");
            $output->write(str_pad($line, $padding = max($padding, strlen($line))));

            sleep(1);
        }

        $output->write("\r");
        $output->writeln(str_pad($message . '.', $padding));
    }

    /**
     * @param InputInterface $input
     *
     * @return Connection
     */
    private function getTargetConnection(InputInterface $input)
    {
        $name = $input->getOption('connection') ?: 'default';
        $service = 'doctrine.dbal.' . $name . '_connection';

        $container = $this->getContainer();

        if ($container->has($service)) {
            if (($instance = $container->get($service)) instanceof Connection) {
                return $instance;
            }

            throw new InvalidArgumentException(sprintf('The service %s is not an instance of "Doctrine\\DBAL\\Connection"', $service));
        }

        throw new InvalidArgumentException(sprintf('Could not find a database connection with the name "%s"', $name));
    }

    /**
     * @param InputInterface $input
     *
     * @return Connection
     */
    private function getSourceConnection(InputInterface $input)
    {
        $params = [
            'dbname'   => $input->getArgument('source'),
            'user'     => $input->getOption('username', null),
            'password' => $input->getOption('password', null),
            'host'     => $input->getOption('host'),
            'port'     => $input->getOption('port', null),
            'driver'   => 'pdo_mysql'
        ];

        return DriverManager::getConnection($params);
    }
}
